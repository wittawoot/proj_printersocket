/*
 Development by Code Development(Thailand) Co.,Ltd
 2018 by Mr.Wittawoot
 */

var escpos=require('lib/escpos');
var socket;

var win = Ti.UI.createWindow({
  backgroundColor: 'white'
});

var btn1 = Ti.UI.createButton({
    title: 'Print to Thermal Printer',
    backgroundColor: "blue",
    top: 50,
    width:'80%',
    height:50,
    color:'FFF',
    borderRadius:10
});

var btn2 = Ti.UI.createButton({
    title: 'Disconnect',
    backgroundColor: "red",
    top: 150,
    width:'80%',
    height:50,
    color:'FFF',
    borderRadius:10
});

var label = Ti.UI.createLabel({ text: 'Connecting ...' });
win.add(btn1);
win.add(btn2);
win.add(label);
win.open();


btn1.addEventListener('click', function() {
	//Call
	//var tttt=escpos.boldOn('MOL');
	//    tttt+=escpos.text('HELLO ! Mr.Wittawoot Thongbor');
	 //var tttt='HELLO' ;
	  var aaaa=escpos.boldOn();
	 	  aaaa+='Test Bold !';
	 	  alert(aaaa);
	 	  
	 	//var qrcode=escpos.qrcode('12345');  
		printTCP0(aaaa); 
	
	
	/*
	var tttt=escpos.text('HELLO ! Mr.Wittawoot Thongbor ab121');        
	printTCP(textX(),textY(),barCODE(),tttt);
	*/
	
	setTimeout(function(){ //get version from server to chech function
		socket.close();
		label.text='Success !';
	}, 1500);
	
	setTimeout(function(){ //get version from server to chech function
		label.text=' -- ESC/POS : Thermal Printer --';
	}, 3000);

});

btn2.addEventListener('click', function() {
	var text=textX();
Ti.Stream.write(socket, text);
alert('Write');
});

/*
 * เอกสารESC/POS 
 * https://www.epson.co.th/troubleshooting-pos
 * font page
 * https://www.manualslib.com/manual/957131/Epson-Tm-T70ii.html?page=129
 * https://reliance-escpos-commands.readthedocs.io/en/latest/font_cmds.html
 * https://reliance-escpos-commands.readthedocs.io/en/latest/imaging.html
 * */
function textX(){
	
	//convenience method
var chr = function(n) { return String.fromCharCode(n); };

    var esc = '\x1B'; //ESC byte in hex notation	
	var newLine = '\x0A'; //LF byte in hex notation
	var cmds = esc + "@"; //Initializes the printer (ESC @)
    
      	//Set font page by hex mode
 		//cmds+='\x1B';
        //cmds+='\x74';
        //cmds+='\x14';  //14 is 20 thaimodel
        
         //cmds+=chr(27) + chr(116) + chr(21);   //Set font page by Decimal mode
         //cmds+=chr(27) + chr(82) + chr(2);   //Set font page by Decimal mode
        
        
       //http://www.kostis.net/charsets/cp874.htm
          //var myString = 'HELLO:'+'\xA2'+'\x4F'+'\x47'+'ก'+'\xA0'+'\xA2'+'\x95'; //กOG   //HEX
        //var myString =chr(149)+ chr(201)+ chr(161)+'|'+'z'+'ไทย';    //Decimal
        // var myString  ;    //Decimal
         /*
         for(i=0;i<3000;i++){
         	myString +=chr(i) ;    //Decimal
         }
         */
        
         var myString  ='กขคง';    //Decima
 
         cmds += esc + '!' + '\x38'; //Emphasized + Double-height + Double-width mode selected (ESC ! (8 + 16 + 32)) 56 dec => 38 hex
		 //cmds += esc+'t'+'\x20';  //(select character code table)
		 cmds+=chr(27) + chr(116) + chr(26);   //Set font page by Decimal mode
         cmds+=chr(27) + chr(82) + chr(0);   //Set font page by Decimal mode

		cmds += myString+' Z-COFFEE'; //text to print
		//cmds += myString; //text to print
		cmds += newLine + newLine;
		
		cmds += esc + '!' + '\x00'; //Character font A selected (ESC ! 0)
        cmds += 'COOKIES['+chr(161)+chr(206)+']                5.00'; 
        cmds += newLine;
        cmds += 'MILK 65 Fl oz             3.78';
        cmds += newLine + newLine;
        cmds += 'SUBTOTAL                  8.78';
        
       
  
       // cmds += newLine + newLine+ newLine+newLine+newLine;
       // cmds +='\x1D'+'\x56'+'\x00';
      

	return cmds;
 }                 


function textY(){
	
	var chr = function(n) { return String.fromCharCode(n); };
	
            var esc = '\x1B'; //ESC byte in hex notation	
	      var newLine = '\x0A'; //LF byte in hex notation
	
        var cmdx= esc + "@"; //Initializes the printer (ESC @)
       
        cmdx += newLine;
        cmdx += 'TOTAL                     9.22';
        cmdx += newLine;
        cmdx += 'CASH TEND                10.00';
        cmdx += newLine;
        cmdx += 'CASH DUE                  0.78';
        cmdx += newLine + newLine;
        cmdx += esc + '!' + '\x18'; //Emphasized + Double-height mode selected (ESC ! (16 + 8)) 24 dec => 18 hex
        cmdx += 'Power by : Code Development Co.,Ltd';
        cmdx += esc + '!' + '\x00'; //Character font A selected (ESC ! 0)
        cmdx += newLine + newLine;
        cmdx += 'THX';
        cmdx += newLine + newLine;
        cmdx += newLine + newLine+ newLine+newLine+newLine;
        
        //cmdx+=chr(28) + chr(112) + chr(1)+ chr(0); //LOGO
        
        //cmdx +='\x1D'+'\x56'+'\x00';  //ตัดกระดาษ
        //cmdx+=chr(10) + chr(27) + chr(109);
        

	return cmdx;
 }        

/*
 *EPSON BARCODE COMMAND
 * http://www.epson.ru/es/upload/ManualTypes/102064/html/apspe_3.htm
 * */
function barCODE(){
		var code = 'A1234567890B';

//convenience method
var chr = function(n) { return String.fromCharCode(n); };

var barcode_ = '\x1D' + 'h' + chr(100) +   //barcode height
    '\x1D' + 'f' + chr(5) +              //font for printed number
    '\x1D' + 'k' + chr(73) + chr(code.length) + code + chr(0)
     +chr(10) + chr(27) + chr(109);  //ตัดกระดาษ
 
 //var barcode ='\x1d\x6b\x08\x7b\x42\x43\x6f\x64\x65\x20\x31\x32\x38\x00'   ;
   var barcode='\x02\x05\x56\x01\x03'  ;
   //var barcode   ='\x0a'+'\x1c\x7d\x25' +'\x1C'+'\xhttps://pyramidacceptors.com'+'\x0a';
    //barcode+=chr(10) + chr(27) + chr(109);  //ตัดกระดาษ
    return  barcode;
}

function printTCP0(text){
	
	socket = Ti.Network.Socket.createTCP({
    host: '192.168.0.222', port: 9100,
    connected: function (e) {
        Ti.API.info('Socket opened!');
        label.text='Socket opened!';
        Ti.Stream.pump(e.socket, readCallback, 1024, true);
        
        

		var myBuffer = Ti.createBuffer({
		    value:text
		});
		
        Ti.Stream.write(socket, myBuffer, writeCallback);
       
     
        
    },
        error: function (e) {
        Ti.API.info('Error (' + e.errorCode + '): ' + e.error);
    },
});socket.connect();
}


function printTCP(text1,text2,text3,text4){
	
	socket = Ti.Network.Socket.createTCP({
    host: '192.168.0.222', port: 9100,
    connected: function (e) {
        Ti.API.info('Socket opened!');
        label.text='Socket opened!';
        Ti.Stream.pump(e.socket, readCallback, 1024, true);
        
        

		var myBuffer = Ti.createBuffer({
		    value:text1
		});
		
        Ti.Stream.write(socket, myBuffer, writeCallback);
       
        	
        Ti.Stream.write(socket, Ti.createBuffer({
            value: text2
        }), writeCallback);
        
       
       /*
         Ti.Stream.write(socket, Ti.createBuffer({
            value: text3
        }), writeCallback);
        */
        
        
        Ti.Stream.write(socket, text4, writeCallback);
        
         alert(myBuffer+text2+text3+text4);
    
       
       
       var chr = function(n) { return String.fromCharCode(n); };
       var cut=chr(10) + chr(27) + chr(109);  //ตัดกระดาษ
       Ti.Stream.write(socket, Ti.createBuffer({
            value: cut
        }), writeCallback);
        
        
        
    },
        error: function (e) {
        Ti.API.info('Error (' + e.errorCode + '): ' + e.error);
    },
});
socket.connect();
}

function writeCallback(e) {
    Ti.API.info('Successfully wrote to socket.');
    
}

function readCallback(e) {
    if (e.bytesProcessed == -1)
    {
        // Error / EOF on socket. Do any cleanup here.
        //socket.close();
       
    }
    try {
        if(e.buffer) {
            var received = e.buffer.toString();
            Ti.API.info('Received: ' + received);
        } else {
            Ti.API.error('Error: read callback called with no buffer!');
        }
    } catch (ex) {
        Ti.API.error(ex);
    }
}